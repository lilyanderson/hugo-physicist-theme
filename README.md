# Physicist Hugo Theme
This project is a theme for the [Hugo static site generator](https://gohugo.io). I am primarily developing this theme for my own website, and as such it is likely to change to suit my needs.
